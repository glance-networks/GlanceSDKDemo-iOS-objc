//
//  main.m
//  SDKDemo_objc
//
//  Created by Kyle Shank on 3/16/18.
//  Copyright © 2018 Glance Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
