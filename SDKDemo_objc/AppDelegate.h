//
//  AppDelegate.h
//  SDKDemo_objc
//
//  Created by Kyle Shank on 3/16/18.
//  Copyright © 2018 Glance Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

