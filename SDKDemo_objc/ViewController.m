//
//  ViewController.m
//  SDKDemo_objc
//
//  Created by Kyle Shank on 3/16/18.
//  Copyright © 2018 Glance Networks. All rights reserved.
//

#import "ViewController.h"

#import <SafariServices/SafariServices.h>
#import <Glance_iOS/Glance_iOS.h>

// Only required to test the custom agent viewer
#import "CustomViewerWindow.h"

// Visitor demo implementation is below. NOTE:
// before reading through this code, make sure you've read through the README

#define GLANCE_GROUP_ID 15687  // Get this from Glance
// Uncomment VISITOR_ID to enable Presence code
// If you are not using Presence you do not need to implement any code within #ifdef VISITOR_ID
//#define VISITOR_ID @"123four"  // This is your identifier for the logged in (or identified) user.

// Make sure your interface implements GlanceVisitorDelegate
// Implement GlanceCustomViewerDelegate to test the custom agent viewer
@interface ViewController () <GlanceVisitorDelegate, GlanceCustomViewerDelegate>
@property (weak, nonatomic) IBOutlet UIButton* startButton;
@property (weak, nonatomic) IBOutlet UIButton* endButton;
@property (weak, nonatomic) IBOutlet UILabel* sessionLabel;
@property (weak, nonatomic) IBOutlet UILabel *presenceIndicator;

// custom agent viewer
@property (nonatomic, strong) UIWindow* customWindow;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Make sure the GlanceVisitor is listening for events on this ViewController
    [GlanceVisitor addDelegate:self];
    
    // Uncomment these lines to use your own custom UI, including custom agent video viewer
    //[GlanceVisitor defaultUI: NO];
    //[GlanceVisitor setCustomViewerDelegate:self];
    
    // For testing on staging server
    // [[[GlanceSettings alloc] init] set: kGlanceSettingGlanceServer value: @"www.myglance.net"];
    
    // Configure Glance Visitor SDK, only call init once, could be done in application:didFinishLaunchingWithOptions:
    #ifdef VISITOR_ID
    // Init with visitor id for Presence
    [GlanceVisitor init:GLANCE_GROUP_ID token:@"" name:@"" email:@"" phone:@"" visitorid: VISITOR_ID];
    #else
    [GlanceVisitor init:GLANCE_GROUP_ID token:@"" name:@"" email:@"" phone:@""];
    #endif
}

- (void)viewDidAppear:(BOOL)animated {
#ifdef VISITOR_ID
    [GlancePresenceVisitor presence:@{@"url": @"main view"}];
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)startSession:(id)sender{
    // Start session and let Glance generate a session key
    [GlanceVisitor startSession];
}

-(IBAction)endSession:(id)sender {
    // End Glance session
    [GlanceVisitor endSession];
}

-(IBAction)openWebBrowser:(id)sender {
    NSURL* url = [NSURL URLWithString:@"https://www.glance.net"];
    SFSafariViewController* vc = [[SFSafariViewController alloc] initWithURL:url];
    [self presentViewController:vc animated:YES completion:nil];
    #ifdef VISITOR_ID
    [GlancePresenceVisitor presence:@{@"url": @"webview https://www.glance.net"}];
    #endif
}

-(void)_sessionStarted:(NSString*)sessionKey {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.startButton setHidden:YES];
        [self.endButton setHidden:NO];
        // Adds a masked view to sessionLabel which will keep it from being seen by the agent
        [GlanceVisitor addMaskedView:self.sessionLabel];
        self.sessionLabel.text = sessionKey;
    });
}

-(void)_sessionEnded {
    dispatch_async(dispatch_get_main_queue(), ^{
        // Remove masked view
        [GlanceVisitor removeMaskedView:self.sessionLabel];
        [self.endButton setHidden:YES];
        self.sessionLabel.text = @"";
        #ifndef VISITOR_ID
        [self.startButton setHidden:NO];
        #endif
    });
}

// Alert
- (void)alertWithTitle:(NSString *)title message:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* a = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:a];
        
        [self presentViewController:alert animated:true completion:nil];
    });
}

// Glance Visitor delegate
-(void)glanceVisitorEvent:(GlanceEvent*)event{
    switch (event.code) {
            
        #ifdef VISITOR_ID
        // implement this code only if using Presence (One-Click)
        case EventVisitorInitialized:
            dispatch_async(dispatch_get_main_queue(), ^{
                [GlancePresenceVisitor connect];
            });
            break;
            
        case EventPresenceConnected: {
            // show some UI if you wish, normally not needed
            dispatch_async(dispatch_get_main_queue(), ^{
                self.startButton.hidden = true;
                self.presenceIndicator.hidden = false;
            });
            break;
        }
            
        case EventPresenceConnectFail:
            NSLog(@"Presence connection failed (will retry): %@", event.message);
            break;
            
        case EventPresenceShowTerms:
            // You only need to handle this event if you are providing a custom UI for terms and/or confirmation
            NSLog(@"Agent signalled ShowTerms");
            break;
            
        case EventPresenceBlur:
            // This event notifies the app that the visitor is now using another app or website
            break;
        #endif
            
        case EventConnectedToSession:
            // Show any UI to indicate the session has started
            // event.properties includes the sessionKey which can be
            // displayed to the user to read to the agent
            [self _sessionStarted: event.properties[@"sessionkey"]];
            break;
            
        case EventStartSessionFailed:
            [self alertWithTitle: @"Start Session Failed" message: event.message];
            [self _sessionEnded];
            break;

        case EventSessionEnded:
            // Show any UI to indicate the session has ended
            [self _sessionEnded];
            break;

        default:
            // Best practice is to log all other events of type EventError, EventWarning, or EventAssertFail
            switch (event.type) {
                case EventError:
                    [self alertWithTitle: @"Error" message: event.message];
                    break;
                    
                case EventAssertFail:
                    [self alertWithTitle: @"Assert Fail" message: event.message];
                    break;
                    
                case EventWarning:
                default:
                    break;
            }
            break;
    }
}

#pragma mark - GlanceCustomViewerDelegate

// CUSTOM AGENT VIEWER - only needed when default UI is not used and customizing the agent video viewer

/**
 * Called when the agent viewer starts with a supplied UIView and size.
 *
 * @param glanceView    UIView displaying agent video.  Add this view to your interface.
 * @param size          Preferred size of the UIView
 */
-(void) glanceViewerDidStart:(UIView*)glanceView size:(CGSize)size{
    glanceView.layer.cornerRadius = 8.0;
    glanceView.clipsToBounds = YES;

    dispatch_async(dispatch_get_main_queue(), ^{
        if(self.customWindow != nil){
            [self.customWindow resignKeyWindow];
            self.customWindow = nil;
        }

        self.customWindow = [[CustomViewerWindow alloc] initWithFrameAndView:[UIScreen mainScreen].bounds view:glanceView size:size];
    });
}

/**
 * Called when the agent viewer has stopped
 *
 * @param glanceView    UIView displaying agent video.  Remove this view from your interface.
 */
-(void) glanceViewerDidStop:(UIView*)glanceView{
    if (self.customWindow != nil){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.customWindow resignKeyWindow];
            [self.customWindow setHidden:YES];
            self.customWindow = nil;
        });
    }
}

@end
