//
//  CustomViewerWindow.m
//  SDKDemo_objc
//
//  Created by Kyle Shank on 4/27/18.
//  Copyright © 2018 Glance Networks. All rights reserved.
//

#import "CustomViewerWindow.h"

@interface CustomViewerWindow ()
@property (nonatomic, assign) UIView* agentView;
@end

@implementation CustomViewerWindow

-(id)initWithFrameAndView:(CGRect)frame view:(UIView*)agentView size:(CGSize)size{
    if (self = [super initWithFrame:frame]){
        self.agentView = agentView;
        self.agentView.frame = CGRectMake(frame.size.width - 10.0 - size.width, frame.size.height - 10.0 - size.height, size.width, size.height);
        
        self.windowLevel = UIWindowLevelAlert;
        self.rootViewController = [[UIViewController alloc] init];
        self.rootViewController.view.frame = frame;
        self.rootViewController.view.backgroundColor = [UIColor clearColor];
        [self.rootViewController.view addSubview: self.agentView];
        
        UIPanGestureRecognizer* drag = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleGlanceViewerDrag:)];
        [self.agentView addGestureRecognizer:drag];
        self.agentView.userInteractionEnabled = true;
        
        [self setHidden: NO];
    }
    return self;
}
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    if (CGRectContainsPoint(self.agentView.frame, point)){
        return self.agentView;
    }
    return nil;
}

- (IBAction)handleGlanceViewerDrag:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:self.rootViewController.view];
    
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.rootViewController.view];
}

@end
