//
//  CustomViewerWindow.h
//  SDKDemo_objc
//
//  Created by Kyle Shank on 4/27/18.
//  Copyright © 2018 Glance Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomViewerWindow : UIWindow

-(id)initWithFrameAndView:(CGRect)frame view:(UIView*)agentView size:(CGSize)size;

@end
