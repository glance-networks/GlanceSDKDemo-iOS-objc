# One-click Connect
## _Visitor Presence and Signaling API_

## Introduction
Glance Presence and Signalling allows an identified iOS application user to notify
agent CRM or support systems that the user is actively using the app.  The agent side
may then signal the app to start screensharing.  The agent can initiate and join the
screenshare automatically or with one-click.

This document is a Quick-start guide for integrating One-click Connect Screensharing
into an iOS application.  It assumes you have already integrated the Glance iOS SDK
(Framework) as described in the [README](README.md) file.

This functionality is available starting in version 4.5.0 of the SDK.
For full documentation on the Presence Visitor API see ****.

**Note:** The version of the document on Gitlab contains the latest version of this document. Your local readme file contains documentation specific to the version you cloned. If you read about features not available in your local version, update your SDK to the latest version on Gitlab.


## Integration
Integration uses the `GlanceVisitor` and `GlancePresenceVisitor` classes.  All methods
must be called on the main (UI) thread.

### Visitor Id
The Visitor Id is a string that uniquely identifies a user connected to the One-Click service.  It is typically
an account or user id associated with an authenticated user.  The same visitor id would be used
when the same visitor is logged into the app on another device, or in a web application instrumented for Glance One-click Cobrowse.

Only one user can be signalable by the One-Click service at a time with a given Visitor Id.  
If two application instances connect using the same Visitor Id, the most recently active
application has "focus" and will receive any signals sent by the Agent.
#### Specifying the Visitor Id

In your call to `GlanceVisitor init:` add the additional parameter `visitorid`.

```objc
    // Configure Glance Visitor SDK
    [GlanceVisitor init:GLANCE_GROUP_ID token:@"" name:@"" email:@"" phone:@"" visitorid:@"123456"];
}
```

An application may call `GlanceVisitor init:` multiple times to specify a Visitor Id or not,
depending on whether the user is logged into the application.  Typically an application will call
`init` once on application startup without a Visitor Id, and again upon user login.

#### Starting Sessions once a Visitor Id has been specified
If a Visitor Id has been specified, it will be the default key for any screensharing session.
Calling `GlanceVisitor startSession:` with no key specified will start a session using the Visitor
Id as the key.  This allows an Agent who knows the identity of the visitor to join the session without the Visitor having to communicate a session key.

If using the Default UI, the session key will not be displayed to the user in the case where a Visitor Id
was used as the session key, since it is not necessary for the user to see or communicate the key.

With a Visitor Id specified, it is still possible to start a session with a random key by passing
@"GLANCE_KEYTYPE_RANDOM" as the session key to the call to `startSession`.  A best practice is to provide
some way in the user interface to start a session with a random key as a fallback, in case the Agent is unable to locate the Visitor's record in the CRM.

### Events
Presence events are delivered to your `GlanceVisitor glanceVisitorEvent:` delegate
method.  Note that events are delivered on the SDK event handling thread, NOT the
main (UI) thread.

You may want to handle these:

```objc
        case EventPresenceConnected:
            NSLog(@"Presence connected");
            break;

        case EventPresenceConnectFail:
            NSLog(@"Presence connect failed: %@", event.message);
            break;

        case EventPresenceSendFail:
            NSLog(@"Presence send failed: %@", event.message);
            break;
```

### Starting and Stopping
To start Presence after receiving EventVisitorInitialized call `connect`

```objc
    [GlancePresenceVisitor connect];
```

To stop (normally on application exit) call `disconnect`

```objc
    [GlancePresenceVisitor disconnect];
```

### Navigation Information
When Presence is connected, information can be sent to the agent side
whenever the user navigates through the app.

```objc
    [GlancePresenceVisitor presence:@{@"url": @"Product page 1"}];
```

When your app becomes active or enters the background state the SDK will also
notify the agent side.

### Local Notifications

Presence has the ability to alert the user when the app enters the background state. If the agent reaches out to your app while it is backgrounded, a local notification can be triggered by the SDK. This will require the notification permission from your app. You can implement this yourself by asking the user, or you can use the following:

```objc
    [GlancePresenceVisitor connect:YES];
```

which, on connecting to Presence, will prompt the user to allow push notifications (if they haven't already accepted). Once accepted, notifications will be sent to the user when the app is in the background and an agent attempts to make contact with the user.

### Customizing the UI
The SDK provides a default user interface to request confirmation and
show Terms and Conditions.  The URL to a Terms web page is configured in the
agent-side CRM system.

To provide your own UI, after calling `connect` call:

```objc
    [GlancePresenceVisitor setDefaultUI:NO];
```

Then handle the `EventPresenceShowTerms` event.
In your event handing you should:
1. Display your terms and conditions and options to accept (start screenshare) or decline.
2. Notify the agent terms have been displayed
3. Notify the agent of accept or decline
4. On accepting, call `GlanceVisitor startSession`

An example:

```objc
        case EventPresenceShowTerms:
            [self myCustomUI: event.properties[@"termsurl"]];
            break;
```

```objc
 -(void)myCustomUI:(NSString *)termsURL
 {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController* actionSheet = [UIAlertController alertControllerWithTitle:@"Allow the agent to view this app?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [GlancePresenceVisitor signalAgent:@"terms" map:@{@"status": @"accepted"}];
            [GlanceVisitor startSession];
        }]];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [GlancePresenceVisitor signalAgent:@"terms" map:@{@"status": @"declined"}];
        }]];

        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Show Terms" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self myShowTerms: termsURL];  // you can use the termsURL or not
        }]];

        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:actionSheet animated:YES completion:nil];
        [GlancePresenceVisitor signalAgent:@"terms" map:@{@"status": @"displayed"}];
    });
 }

```
