# Glance SDKs
## Release Notes

### Changes in 4.8.5

**iOS**
- Fix potential crash on initialization of framework

**Android**
- Add orange border to default UI during session (parity with iOS)
- Fix EventChildSessionEnded not posted when video ended
- Fix UI remains visible after ending video then main session

### Changes in 4.8.2

**iOS**
- Fix potential crash when force-quitting application

**Android**
- Fix occasional freeze of agent video (video from agent on browser works better with Chrome 78 installed on the Android device)
- Fix intermittent crash with large number of event listeners
- Improve display of terms and conditions on default user interface 
- Fix TLS issue which could cause an SSLContext initialization error.  Android SDK now supports TLS 1.3 on devices using Android 10+


### Changes in 4.8.1

**iOS**
- Update to Twilio SDK 2.1
- Minor fixes to default UI

**Android**
- Update to Twilio SDK 2.1
- Automatically scale capture better for high or low density displays
- Add StartParams `scale` parameter to manually adjust capture scaling

### Changes in 4.7.2

**iOS and Android**
- 15020 Allow calling Visitor.init again with visitor id
- Small fixes to Visitor and Presence

**Mac**
- 14943 Build with XCode 10.2.1 for Swift ABI compatibility

### Changes in 4.7.1

**iOS and Android**
- A default UI for screenshare, video and voice is now available*
- GlanceVoice class moved from separate SDK to main SDK*

**iOS**
- initVisitor method supplied, callable from Swift 5*

**All platforms**
- Agent video can be shown by an agent connecting with Chrome (Windows SDK requires your app to include an embedded browser)
- 14874 Underscore is now allowed in a Presence visitor id
- 14430 For manually started session, different key from Presence visitor id can be specified


### Changes in 4.5.1

**All platforms**
- 14180 Presence/One-Click session start by agent on Salesforce
- small fixes to Presence timing and edge cases of start session/show terms prompt

**iOS**
- 14075 Glance Glance gesture window should not become key window
- 14143 add setGlanceWindowTag: method to set window tag of gesture window*
- 14258 add setContentMode method to set UIViewContentMode of agent video viewer*

### Changes in 4.5.0

**All platforms (except Mac):**
- Implementation of PresenceVisitor for Signaling and agent One-Click session connect (NOTE: this release does not yet support Glance for Salesforce and optional argument passing to invokeVisitor in the Presence Agent API)

**Android**
- 14099 Support for custom vector drawable Agent cursor 

### Changes in 4.4.4

**Android**
- 13781 Replace libpng, libcurl, libssl, libcrypto with native code, reducing SDK size
- Fix support for TLS 1.2 on older OS versions
- Bug fixes, especially improve stability of agent video

**Windows**
- 13903 Agent Video window remains on top
- 13721 Fixes to capture with monitor scaling past 100%

**Mac**
- 13903 Agent Video window remains on top
- 13630 Fix scroll wheel doesn't scroll on Mac when viewing from browser viewer
- 13414 Replace deprecated class/method/function usages


### Changes in 4.4.3

**iOS**
- 13679 Fix crash adding delegate in test environment without autorelease pool

**All platforms:**
- 13655 Fix EventVisitorInitialized posted before Init is finished
- 13152 Don't start event thread until SDK is used

### Changes in 4.3.1

**iOS**
- 13210 Update for iOS 12 change that affected performance
- 12841 Fix to keyboard masking in Visitor Sessions
- 12414 Add Visitor.pause method *
- Bug fixes

**Android**
- 12842 Masking by view ID *
- 12843 Visitor session start with StartParams and full screen support *
- 13400 Support for custom agent video viewer *
- Bug fixes

### Changes in 4.1.1 (iOS only)

**iOS**
- 12414 Add pause method to Visitor API *
- 12435 Pause/Resume screensharing when app resigns/becomes active app
- 12440 Mask fields even when hidden, due to edge/race condition
- 12433 Fix Memory leak in HttpRequest of NSMutableURLRequest
- 12429 Ensure idleTimerDisabled is run on main thread
- 12438 Fix crash when suspending app on iPad Mini (iOS 9.3.5)


### Changes in 4.1.0

**iOS**
- 12104 Replace deprecated CFNetwork library for HTTPS
- 12151 Keyboard masking method *
- 12172 Fix: Gesture pointer on customer side is slightly to the right of where the agent is pointing.
- 12326 Remove "assign" property for customViewerDelegate *

**Android**
- 12148 Document per-architecture builds using build.gradle "splits" declaration
- 12149 Update deprecated Gradle dependencies
- 12150 Eliminate Glide source files
- 12195 Make EventType public and accessible *
- 12307 Remove use of Kotlin (rewrite in Java)  
- Fix several small bugs in Agent Video  

**All platforms:**
- 12140 Upgrade libpng to 1.6.34 [zlib and jpeglib already at current version]
- Add several checks against incorrect API usage

 &#42; API Changes
 
### Changes in 4.0
 
 **All platforms**
 - Simple Visitor API
 